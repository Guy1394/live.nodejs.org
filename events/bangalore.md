# Bangalore

* **Date:** March, 19th, 2016
* **Location:** Wipro, Tower S4, 5th floor SJP 2, Auditorium 
* **Address:** Doddakannelli, Sarjapur Road, Bengaluru, Karnataka 560035
* Parking space is limited to 200 vehicles hence it is advised to hire cabs to attend the event.
* All guests are requested to report to SJP2, front office to collect guest passes.

<a class="button" href="https://www.regonline.com/Register/Checkin.aspx?EventID=1813427">Register Now!</a>

## Sponsors

![http://www.wipro.com/](/static/sponsors/WNC_Logo.png "Wipro")

We're still looking for sponsors. <a href="mailto:tbenzies@linuxfoundation.org?subject=Node.js%20Live%20Sponsorship">Contact us</a> if you're interested.

## NodeTogether by ![https://www.npmjs.com](/static/sponsors/npm-logo.svg "npm")

NodeTogether is an educational initiative to improve the diversity of the Node community by bringing people of underrepresented groups together to learn Node.js.

To learn more, and/or if you are interested in applying as a student, mentor, or sponsor, please visit [http://www.nodetogether.org/](http://www.nodetogether.org/).

## Agenda


Time | Topic 
--- | --- 
13:00 | Arrivals 
14:00 | Welcome Address/Main Program
15:00 | 30 Minute Break
16:30 | 30 Minute Break
18:00 | Food/Beverage & Networking
19:30 | Event Concludes

[Let us know](https://github.com/nodejs/live.nodejs.org#interested-in-speaking) 
if you're interested in speaking :)

#### [Ashley Williams](https://github.com/ashleygwilliams)

Ashley, npm's Developer Community and Content Manager, gives a quick tour of npm’s greatest 
features, old and new, and demonstrates how they can be integrated into your workflow to make 
you better, happier, and more productive. Ashley will outline the most commonly used npm tools 
for starting a project, managing a project through development, test, and deployment, and 
managing teams and organization project work. She’ll focus in particular on workflows that 
will help frontend developers, npm’s biggest and fastest-growing group of users.